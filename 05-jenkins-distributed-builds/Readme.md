# Setting Up a Jenkins Master and a Slave on Ubuntu 18
---
## Infrastructure Provisioning with Vagrant and Ansible:
   - Use **Vagrant** to launch two Linux Ubunto/Bionic64 VMs (See the *Vagranfile*). 
     * The first VM acts as the Jenkins Master Node (It plays also the role of Ansible Control node). 
     * The second VM acts as the Jenkins Slave (Agent) Node.
   - Configure SSH in order to enable Ansible to connect to the target VM (Use **ssh-keygen** and **ssh-copy-id** commands).
   - Add the inventory (the **hosts** file in this example) 
      ```shell 
        [local]
        127.0.0.1 ansible_python_interpreter=/usr/bin/python3
        [slave]
        10.0.15.11  ansible_python_interpreter=/usr/bin/python3  
      ```
   - Install the **geerlingguy.jenkins** jenkins role from within _/vagrant_ folder on Master node and  **geerlingguy.jenkins** java role on the Slave node.
      
   - Write the **Ansible** Playbook. An example is provided in *jenkins_master_and_slave.yml*
        
     ```yaml
      ---
      - name: Install master Jenkins
        hosts: local
        connection: local
        become: yes
        roles:
          - geerlingguy.jenkins
        
      - name: Install Java 8 on Slave 
        hosts: slave
        become: true
        roles:
          - geerlingguy.java
     ```      
 - Run the Ansible playbook
      ``` sh
         $ ansible-playbook -i hosts jenkins_master_and_slave.yml
      ```
## Jenkins Master and Slave Configuration

### Step 1: Configure Jenkins Master Credentials
When you got the master server Jenkins installed, we need to configure the master server itself. By default, there are different ways to start Jenkins agent nodes, we can launch the agent nodes through SSH, OS administrative account, and via Java Web Start (JNLP), choose the best way depending on your environment setup and operating system.

For this lab, we will launch the agent nodes through ssh, and we need to setup Jenkins credentials on our master server.
- **Generate SSH Key**
  We will be using the ssh key authentication to setup our agent nodes, so we need to generate the ssh key for the Jenkins user and then upload the key to each server node manually using 'ssh-copy-id'.

  On the Jenkins master server, login to the Jenkins user and generate the ssh key.
  ``` sh 
  $ su - jenkins
  $ ssh-keygen -t rsa
  ```
  And you will get the '*id_rsa*' private and '*id_rsa.pub*' public key in the '*.ssh*' directory.

- **Setup Credentials on Jenkins**
  - Open your Jenkins dashboard and click on the 'Credentials' menu on the left. And click the 'global' domain link.
  - Now click 'Add Credentials'.
  - Now choose the authentication method.
    **Kind**: SSH Username with private key
    **Scope**: Global
    **Username**: jenkins
    **Private key**: Enter directly and paste the '**id_rsa**' private key of Jenkins user from the master server.

### Step 2 - Set up Slave Nodes

- **Verify that Java 8 is installed** (this was accomplished using Ansible).
  ``` sh 
    $ java -version
  ```
- **Add New Jenkins User**
  Now add the 'Jenkins' user to all agent nodes. Run the command below.
   ``` sh 
  $  useradd -m -s /bin/bash jenkins
  $  passwd jenkins  # Give a password to the user jenkins
  ```

- **Copy the SSH Key from Master to Slave**
  We need to upload to each slave nodes using '`ssh-copy-id`' command as below.
  
  ``` sh 
    $ ssh-copy-id jenkins@10.0.15.11  # Type-in the Jenkins user password when asked.
  ```  
  The ssh key '`id_rsa.pub`' has been uploaded to all agent nodes.

### Step 3 - Add New Slave Nodes
- On the Jenkins dashboard, click the 'Manage Jenkins' menu, and click 'Manage Nodes'. Click the 'New Node'.
- Type the node name 'slave01', choose the 'permanent agent', and click 'OK'.
- Now type node information details.
  **Description**: slave01 node agent server
  **Remote root directory**: /home/jenkins
  **Labels**: slave01
  **Launch method**: Launch slave agent via SSH, type the host ip address '10.0.15.11', choose the authentication using 'jenkins' credential (Which has been defined previously).
- Now click 'Save' button and wait for the master server to connect to all agent nodes and launch the agent services.
## Testing the Master/Slave Architecture through a Distributed Pipeline
- Create a New Jenkins Job and set its pipeline as follows. The pipeline involves both the master and the slave : The stages **Build** and **Test** run on the `master` and stage **Deploy** runs on the slave `slave01`.
   ``` groovy 
   pipeline {
  agent { label 'master' }
  stages {
	  stage('Build') {
		steps {
		  echo "This is my Build step"
		  echo "The running node is ${NODE_NAME}"
		}
	  }
	  stage('Test') {
		steps {
		  echo "This is my Test step"
		}
	  }
	  stage('Deploy') {
	      agent { label 'slave01' }
		steps {
		  echo "This is my Deploy step"
		  echo "The running node is ${NODE_NAME}"
		}
	  }
    }
   }
    ```  
- Run the Job and verify that the Stages run the correct node. The Log should look like this:
  ```shell
  Started by user admin
  Running in Durability level: MAX_SURVIVABILITY
  [Pipeline] Start of Pipeline
  [Pipeline] node (hide)
  Running on Jenkins in /var/lib/jenkins/workspace/05-jenkins-distributed-builds
  [Pipeline] {
  [Pipeline] stage
  [Pipeline] { (Build)
  [Pipeline] echo
  This is my Build step
  [Pipeline] echo
  The running node is master
  [Pipeline] }
  [Pipeline] // stage
  [Pipeline] stage
  [Pipeline] { (Test)
  [Pipeline] echo
  This is my Test step
  [Pipeline] }
  [Pipeline] // stage
  [Pipeline] stage
  [Pipeline] { (Deploy)
  [Pipeline] node
  Running on slave01 in /home/jenkins/workspace/05-jenkins-distributed-builds
  [Pipeline] {
  [Pipeline] echo
  This is my Deploy step
  [Pipeline] echo
  The running node is slave01
  [Pipeline] }
  [Pipeline] // node
  [Pipeline] }
  [Pipeline] // stage
  [Pipeline] }
  [Pipeline] // node
  [Pipeline] End of Pipeline
  Finished: SUCCESS
  ``` 