# Jenkins Decalarative Pipeline Syntax / Pipeline Structure
---
Jenkins provides you with two ways of developing your pipeline code: **Scripted** and **Declarative**. 
  - **Scripted pipelines**, also known as "traditional" pipelines, are based on Groovy as their Domain-specific language. 
  - On the other hand, **Declarative pipelines** are recent and hey they provide a simplified and more friendly syntax with specific statements for defining them, without needing to learn Groovy.

## Declarative pipeline syntax
A valid Declarative pipeline must be defined with the "pipeline" sentence 

  ```groovy
  pipeline {
      /* insert Declarative Pipeline here */
  }
  ``` 
- The pipeline includes the following sections:
     -  agent
      - stages
      - stage
      - steps 

- Also, these are the available directives:
    -  environment (Defined at stage or pipeline level)
      - input (Defined at stage level)
      - options (Defined at stage or pipeline level)
      - parallel
      - parameters
      - post
      - script
      - tools
      - triggers
      - when
More on the syntax is provided [in the official Jenkins Site.](https://jenkins.io/doc/book/pipeline/syntax/)

## Agent, Stages, Stage, and Steps
- **Agent**
The agent section specifies where the entire Pipeline, or a specific stage, will execute in the Jenkins environment depending on where the agent section is placed. 
See More on agents [here.](https://jenkins.io/doc/book/pipeline/syntax/#agent) 
- **Stages**
Stages contain a sequence of one or more stage directives, the stages section is where the bulk of the "work" described by a Pipeline will be located. At a minimum, it is recommended that stages contain at least one stage directive for each discrete part of the continuous delivery process, such as Build, Test, and Deploy. 
    ```groovy
    // Jenkinsfile (Declarative Pipeline)
    pipeline {
      agent any
      stages {
        stage('Build') {
        steps {
          echo "This is my first step"
          echo "The build number is ${env.BUILD_NUMBER}"
              echo "You can also use \${BUILD_NUMBER} -> ${BUILD_NUMBER}"
        }
        }
        stage('Test') {
        steps {
          echo "This is my Test step"
        }
        }
        stage('Deploy') {
        steps {
        echo "This is my Deploy step"		}
        }
      }
    }
    ```
See More on stages [here.](https://jenkins.io/doc/book/pipeline/syntax/#stages) 
- **Stage**
The stage directive goes in the stages section and should contain a steps section, an optional agent section, or other stage-specific directives. Practically speaking, all of the real work done by a Pipeline will be wrapped in one or more stage directives. 
    ```groovy
    // Declarative //
    pipeline {
        agent any
        stages {
            stage('Example') {
                steps {
                    echo 'Hello World'
                }
            }
        }
    }
    ```
See More on stage [here.](https://jenkins.io/doc/book/pipeline/syntax/#stages) 
- **Steps**
The steps section defines a series of one or more steps to be executed in a given stage directive.
See More on steps [here.](https://jenkins.io/doc/book/pipeline/syntax/#steps) 