# JenkinsCI : Continious Delivery
---
## The CI pipeline
We are now at the heart of the CD design. We will not create a new pipeline; instead, we
will build on the existing CI Pipeline in Jenkins. The new CD pipeline will
have the following stages:
1. Fetch the code from the version control system (VCS) on a push event
(initialization of the CI pipeline).
2. Build and unit test the code; publish a unit test report on Jenkins.
3. Perform static code analysis on the code and upload the result to SonarQube. Fail
the pipeline if the number of bugs crosses the threshold defined in the quality
gate.
4. Perform integration testing; publish a unit test report on Jenkins.
5. Upload the built artifacts to Artifactory along with some meaningful properties.
6. Deploy the binaries to the testing environment.
7. Execute testing (quality analysis).
8. Promote the solution in Artifactory and mark it as a release candidate.

The purpose of the preceding CD pipeline is to automate the process of continuously
deploying, testing (QA), and promoting the build artifacts in the binary repository.
Reporting for failures/success happens at every step. Let us discuss these pipelines and their
constituents in detail.

The figure below shows the stages of the pipeline.
![Pipeline](images/pipeline.jpg)

## Toolset for Continious Delivery
The example project for which we are implementing CD is a simple Maven project. In
this chapter, we will see Jenkins working closely with many other tools. The following table
contains the list of tools and technologies involved in everything that we will be seeing:
   The example project for which we are implementing CD is a simple Maven project. 
   
   |   Tool | Role |
|---------|------------| 
|**Java**  |Primary programming language used for coding |
|**Maven** |Build tool|
|**JUnit** |Unit testing and integration testing tools |
|**Jenkins**| Continuous Integration tool|
|**GitLab** |Version control system |
|**SonarQube** |Static code analysis tool |
| **JFrog Artifactory** |Binary repository manager|
| **Apache Tomcat** |Application server to host the solution|
| **Apache JMeter** |Performance testing tool|
   
   
## Implementation details

See **Chapter 8: Continuous Delivery Using
Jenkins** of *Nikhil Pathania* Book **Continuous Integration Using Jenkins, 2nd Edition**

![book](images/book.jpg)

